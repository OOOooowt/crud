package com.service;

import com.pojo.Person;

import java.util.List;

public interface TestService {



   public List<Person> find(String username) ;

   int insert(Person person);
}
