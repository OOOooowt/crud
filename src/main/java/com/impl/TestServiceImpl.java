package com.impl;

import com.mapper.TestMapper;
import com.pojo.Person;
import com.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestMapper mapper;
    @Override
    public List<Person> find(String username) {
        Person person = new Person();
        person.setUsername(username);
//        person.setUserid(11);
        List<Person> list = mapper.select(person);
        return list;
    }

    @Override
    public int insert(Person person) {
        return mapper.insert(person);
    }
}
