package com.pojo;

import lombok.Data;

@Data
public class Person {
    private String username;
    private String userid;
}
